from Car import Car
from Ride import Ride
import os


def readFile(fileName):
    f = open(fileName, 'r')
    lines = f.readlines()
    firstLine = lines[0].split(" ")
    carsLength = int(firstLine[2])
    ridesLength = int(firstLine[3])
    bonus = int(firstLine[4])
    steps = int(firstLine[5])

    rides = []
    for i in range(1, ridesLength + 1):
        line = lines[i].split(" ")
        ride = Ride(i - 1,
                    int(line[0]),
                    int(line[1]),
                    int(line[2]),
                    int(line[3]),
                    int(line[4]),
                    int(line[5]),
                    bonus)
        rides.append(ride)

    return {
        'rides': rides,
        'carsLength': carsLength,
        'steps': steps,
        'bonus': bonus
    }


def writeFile(fileName, cars):
    with open(fileName, 'w') as f:
        for car in cars:
            rideIds = list(map(lambda ride: ride.identifier, car.assignedRides))
            if len(rideIds) == 0:
                rideIdsString = ''
            else:
                rideIdsString = ' '.join(str(identifier) for identifier in rideIds)
            print(len(car.assignedRides), rideIdsString, file=f)


def assign(workspace, cars, b):
    workspace.sort(key=lambda r: r.distance, reverse=False)
    maxRidesToAdd = len(cars)
    ridesAdded = 0
    scoreGained = 0
    for ride in workspace:
        minWaitCar = None
        minWaitTime = -1
        for car in cars:
            if car.isCompatible(ride):
                waitTime = car.waitFor(ride, b)
                if minWaitCar is None or minWaitTime >= waitTime:
                    minWaitCar = car
                    minWaitTime = waitTime

        workspace.remove(ride)
        if minWaitCar is not None:
            scoreGained += minWaitCar.addRide(ride, b)
            ridesAdded += 1
            if ridesAdded == maxRidesToAdd:
                return scoreGained
        return scoreGained


def cleanWorkspace(workspace, cars):
    for ride in workspace:
        compatible = False
        for car in cars:
            if car.isCompatible(ride):
                compatible = True
                break
        if not compatible:
            workspace.remove(ride)
    return workspace


def expandWorkspace(rides, workspaceBefore, index, rideIndex):
    workspace = []
    for i in range(rideIndex, index):
        workspace.append(rides[i])
    maxLatestEnd = max(ride.latestEnd for ride in workspace)
    for i in range(0, len(workspaceBefore)):
        workspace.append(workspaceBefore[i])
    for i in range(index, len(rides)):
        if rides[i].earliestStart <= maxLatestEnd:
            workspace.append(rides[i])
        else:
            break
    return workspace


def algo(rides, carsNumber, steps, bonus):
    score = 0
    cars = []
    for i in range(0, carsNumber):
        cars.append(Car())
    rides.sort(key=lambda ride: ride.earliestStart, reverse=False)
    rideIndex = 0
    workspace = []
    while rideIndex < len(rides):
        toAppend = expandWorkspace(rides, workspace, carsNumber - len(workspace), rideIndex)
        for i in range(0, len(toAppend)):
            workspace.append(toAppend[i])
        rideIndex += len(toAppend)
        score += assign(workspace, cars, bonus)
        workspace = cleanWorkspace(workspace, cars)
    return cars


inputFileNames = []

for file in os.listdir("input"):
    inputFileNames.append(os.path.join('input', file))

for fileName in inputFileNames:
    values = readFile(fileName)
    cars = algo(values['rides'], values['carsLength'], values['steps'], values['bonus'])
    writeFile(fileName.replace('input', 'output').replace('.in', '.out'), cars)
