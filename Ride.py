class Ride:
    def __init__(self, identifier, startIntersectionX, startIntersectionY, finishIntersectionX, finishIntersectionY,
                 earliestStart, latestEnd, bonus):
        self.identifier = identifier
        self.startIntersectionX = startIntersectionX
        self.startIntersectionY = startIntersectionY
        self.finishIntersectionX = finishIntersectionX
        self.finishIntersectionY = finishIntersectionY
        self.earliestStart = earliestStart
        self.latestEnd = latestEnd
        self.bonus = bonus
        self.distance = self.findDistance()

    def findDistance(self):
        return abs(self.startIntersectionX - self.finishIntersectionX) + \
               abs(self.startIntersectionY - self.finishIntersectionY)

    def distanceTo(self, other):
        return abs(self.finishIntersectionX - other.startIntersectionX) + \
                abs(self.finishIntersectionY - other.startIntersectionY)
