class Car:
    def __init__(self):
        self.assignedRides = []

    def isCompatible(self, ride):
        return len(self.assignedRides) == 0 or (self.assignedRides[len(self.assignedRides) - 1].end +
                                                self.assignedRides[len(self.assignedRides) - 1].distanceTo(
                                                    ride) <= ride.earliestStart)

    def distanceTo(self, ride):
        if len(self.assignedRides) == 0:
            return abs(ride.startIntersectionX) + abs(ride.startIntersectionY)
        else:
            return self.assignedRides[len(self.assignedRides) - 1].distanceTo(ride)

    def waitFor(self, ride, b):
        if len(self.assignedRides) == 0:
            if self.distanceTo(ride) <= ride.earliestStart:
                return ride.earliestStart - b
            else:
                return self.distanceTo(ride)
        else:
            lastRide = self.assignedRides[len(self.assignedRides) - 1]
            if lastRide.latestEnd + self.distanceTo(lastRide) <= ride.earliestStart:
                return ride.earliestStart - lastRide.end - b
            else:
                return self.distanceTo(ride)

    def addRide(self, ride, b):
        if len(self.assignedRides) == 0:
            previousRideEnd = 0
        else:
            previousRideEnd = self.assignedRides[len(self.assignedRides) - 1]
        ride.start = max(previousRideEnd + self.distanceTo(ride), ride.earliestStart)
        ride.end = ride.start + ride.findDistance()
        ride.car = self
        self.assignedRides.append(ride)
        score = ride.findDistance()
        if ride.start == ride.earliestStart:
            return score + b
        else:
            return score

